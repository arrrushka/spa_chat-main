package kz.aitu.chat.controller;

import kz.aitu.chat.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/participants")
@AllArgsConstructor
public class ParticipantController {
    private ParticipantRepository participantRepository;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(participantRepository.findAll());
    }

}
