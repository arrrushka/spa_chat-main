package kz.aitu.chat.service;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.ChatRepository;
import kz.aitu.chat.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor

public class ChatService {
    private ChatRepository chatRepository;
    private ParticipantRepository participantRepository;

    public List<Users> getUsersByChatId (Long chat_id){
        List<Participant> participants = participantRepository.findAllByChatId(chat_id);
    }

    public List<Chat> findAll (){
        return chatRepository.findAll();
    }
}
